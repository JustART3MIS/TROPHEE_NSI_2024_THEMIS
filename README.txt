# TROPHEE NSI 2024 - Logiciel THEMIS


## Qu'est-ce que THEMIS ?

Thémis est, dans la mythologie grecque, la déesse de l'ordre. C'est pourquoi THEMIS est un logiciel de gestion de personnel et d'astreinte pour les organismes disposant d'un système
d'astreinte. Il permet de gérer les attributions des effectifs sur les périodes de gardes et de 

## Que peut-on faire avec ?

- [x] Voir le calendrier gérant les différentes gardes
- [x] Attribuer des effectifs sur les différentes dates
- [x] Voir les informations des effectifs, les modifier et en ajouter